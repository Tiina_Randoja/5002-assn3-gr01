using System;

namespace ASSN3GR1
{
    public class Cars : IComparable
    {
        public string CarMake { get; set; }
        public string CarModel { get; set; }
    
         int IComparable.CompareTo(object obj)
        {
            Cars c=(Cars)obj;
            return String.Compare(this.CarMake,c.CarMake);
        }  
    }
}
