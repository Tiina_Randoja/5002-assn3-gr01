﻿using System;
using System.Collections.Generic;

namespace ASSN3GR1
{
  
    // Class : Word, for the list of cars in option 4
    class Word
    {  
        public string Input {get; set;}

        public Word (string _input)
            {
                Input = _input;
            }
    }
     

     //Start of main program   
        class Program
        { 
      
            static void Main(string[] args)
            { 
                Console.Clear();

                //The menu with 5 options      
                var keynumber = 0;
          
                do {
                        Console.Clear();
                        Console.WriteLine("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~         Menu             ~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~ Warning:  Prepare to be amazed ~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~        Option 1:         ~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~ A program printing numbers from 99-0 (backwards!) ~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~        Option 2:         ~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~ A program that counts the letters in a word! ~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~        Option 3:         ~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~ A program that counts fruits and vegetables! ~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~        Option 4:         ~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~ A program that lists 10 cars! ~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~        Option 5:         ~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~~  To quit the program   ~~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");


                        //User input for menu options
                        Console.WriteLine("\nChoose an option (Type in the number and press <Enter>) :");                        
                        var choice = Console.ReadLine();
                        bool Inputr = int.TryParse(choice, out keynumber);

                        //Display the option chosen
                        Console.WriteLine($"\nYou chose option number {choice}\n");
    
                        if (Inputr)
                        {
                            switch (keynumber)
                            {       
                                
                                    //Option 1: Print numbers from 99-0
                                    case 1:                 
                                    
                                    //Starting from 100, -1 on every line and print it (as long as the value is greater than 0).
                                    Console.WriteLine("Here's something really fun, numbers from 99-0 in a list : \n");
                                    
                                    for (var w = 100; w > 0; )
                         
                                    {   
                                        w--;
                                        Console.WriteLine($"This is line number {w}"); 
                                    }

                                    
                                    Console.WriteLine("\nPress <Enter> to return to the main menu");
                                    Console.ReadKey();
                                    Console.Clear();
                            
                                    break;

                                    
                                    //Option 2: A program that counts letters in a word inserted by the user
                                    case 2:
                                    Console.Clear();

                                    //Declare variables
                                    var cont = 1;
                                    var i = 0;
                                    var startStop = "";
                                    
                                    //Create a list
                                    List <Word> wordList = new List<Word>();

                                    Console.Clear();

                                    //Ask the user to insert a word
                                    while(cont>0)
                                    {
                                    
                                        Console.WriteLine("Please type a word and press enter.");
                                        
                                        wordList.Add(new Word(Console.ReadLine()));
                                        i++;
                                        
                                        //Give the user the option to add another word
                                        Console.WriteLine("Would you like to add another?");
                                        Console.WriteLine("type y/n then press enter");
                                        startStop = Console.ReadLine();

                                        if(startStop == "y")
                                        {
                                            Console.Clear();
                                        }
                                        else
                                        {
                                            cont = 0;
                                        }
                                    }

                                    Console.Clear();
                                    
                                    //Set the original value to 0                            
                                    i = 0;                           
                                    Console.WriteLine("The word(s) you chose are:");
                                    
                                    //Add +1 for every letter detected and print the results
                                    foreach(Word element in wordList)
                                    {
                                        Console.WriteLine($"{wordList[i].Input} is {wordList[i].Input.Length} characters long.");
                                        i++;
                                    }

                                    Console.WriteLine("");
                                    Console.WriteLine("Press <Enter> to return to the main menu");
                                    Console.ReadKey();
                                    
                                    break;
                                    
                                    
                                    //Option 3: Display 10 fruit and vegetables, count how many fruit/vegetables there is and print the fruit
                                    case 3:
                                    Console.Clear();

                                    //List of fruit
                                    var dict= new Dictionary<string,string>();
                                    dict.Add("Potatoe","Vegetable");
                                    dict.Add("Banana","Fruit");
                                    dict.Add("Apple","Fruit");
                                    dict.Add("Carrot","Vegetable");
                                    dict.Add("Pumpkin","Vegetable");
                                    dict.Add("Pear","Fruit");
                                    dict.Add("Strawberry","Fruit");
                                    dict.Add("Plum","Fruit");
                                    dict.Add("Fejoa","Fruit");
                                    dict.Add("Broccoli","Vegetable");

                                    //Declare integers as 0
                                    int count1 = 0;
                                    int count2 = 0;
                                    
                                    //Add +1 every time the value is "Fruit"
                                    foreach (var z in dict)
                                    {
                                        if (z.Value == "Fruit")
                                        {
                                            count1++;
                                        }
                                        
                                        //Add +1 every time the value is "Vegetable"
                                        else if (z.Value == "Vegetable")
                                        {
                                            count2++;
                                        }
                                    }

                                    //Print results
                                    Console.WriteLine($"\nNumber of Fruit :{count1}");
                                    Console.WriteLine($"Number of Vegatables :{count2}\n");


                                    Console.WriteLine("List of Fruit we have \n");
                                    foreach (var x in dict)
                                    {
                                        if  (x.Value=="Fruit")
                                        {
                                            Console.WriteLine($"{x.Key}");
                                        }
                                    }
                
                                    Console.WriteLine("Press <Enter> to return to the main menu");
                                    Console.ReadKey();

                                    break;
                                    
                                    
                                    //Option 4: A list with 10 car makes and models
                                    case 4:
                                    Console.Clear();
                                    
                                    List<Cars> car = new List<Cars>();
                                    car.Add(new Cars{ CarMake = "Toyota", CarModel = "Yaris"});
                                    car.Add(new Cars{ CarMake = "Ford", CarModel = "Focus"});
                                    car.Add(new Cars{ CarMake = "Hyundai", CarModel = "i20"});
                                    car.Add(new Cars{ CarMake = "Ford", CarModel = "Fiesta"});
                                    car.Add(new Cars{ CarMake = "Citroen", CarModel = "C3"});
                                    car.Add(new Cars{ CarMake = "Mini", CarModel = "Cooper"});
                                    car.Add(new Cars{ CarMake = "Audi", CarModel = "A4"});
                                    car.Add(new Cars{ CarMake = "Ford", CarModel = "Ranger"});
                                    car.Add(new Cars{ CarMake = "Isuzu", CarModel = "Bighorn"});
                                    car.Add(new Cars{ CarMake = "Honda", CarModel = "City"});

                                    //Sort CarMake by alphabetical order using IComparable in Class
                                    car.Sort();
                                    Console.WriteLine("\nList - Sorted by Make \n");
                                    
                                    //print sorted list to screen
                                    foreach(Cars c in car)
                                    {
                                        Console.WriteLine(c.CarMake + "\t\t" + c.CarModel);
                                    }
                                    
                                    Console.WriteLine("Press <Enter> to return to the main menu");
                                    Console.ReadKey();
                                    break;
                                    
                                    
                                    //Option 5: Exit the program
                                    case 5:
                                    Console.Clear();
                                    Console.WriteLine("Thank you for using the program");
                                    break;

                                    default:
                                    //Ask user to input appropriate number
                                    Console.WriteLine($"Option {choice} does not exist. Please restart the program and insert a number between 1-5.");
                                    break;

                            }
                        }
                
                        //If the user didnt type in a number
                        else
                        {
                            Console.WriteLine("You didn't type in a number");
                            keynumber = 0;
                            Console.WriteLine("Press <Enter> to quit the program");
                            Console.ReadKey();
                        }
                   }                   
                   while(keynumber < 5);

                   //Closing 
                   Console.WriteLine();
                   Console.WriteLine("Press <Enter> to quit the program");
                   Console.ReadKey();
                     
            }
        }
    }


   


            


  

